package com.devcamp.j01_javabasic.s40;

public class Voucher {
    public static void main(String args[]) { //Tạo class Voucher trong package com.devcamp.j01_javabasic.s40 với method public static void main(String[] args)
        Voucher.showVoucher("V0023"); //Gọi method showVoucher() trong method main
    }
    //Tạo method “showVoucher()” với tham số voucherCode. Cho phép in ra nội dung: “This is voucher code: V0023”. trong đó “V0023” lấy từ tham số voucherCode.
    public static void showVoucher(String voucherCode) {
        System.out.println("This is voucher code: " + voucherCode);
    }
}
